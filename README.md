#Social plugin (Silverstripe 4)

- Facebook feed (developing)

## Set up access to private bitbucket repository

Create `auth.json` in `.composer` folder

eg:
```
/Users/artyom/.composer/auth.json
```

with following:
```
{
    "bitbucket-oauth": {
        "bitbucket.org": {
            "consumer-key": "",
            "consumer-secret": ""
        }
    }
}
```

Create consumer to get consumer-key and consumer-secret: https://confluence.atlassian.com/bitbucket/oauth-on-bitbucket-cloud-238027431.html


## Installation
Update composer.json of your project by adding `repositories`

```
{
    ...
    ...
    ...

    "repositories": [
      {
        "name": "swordfox-social",
        "type": "git",
        "url": "git@bitbucket.org:swordfox/swordfox-social.git"
      }
    ]
}
```

run

```
composer require swordfox-social dev-master
```

and don't forget

```
dev/build
```
