<% if status %>

  <% if countposts %>

    <% loop posts %>
      <%--
        Description:  $Description
        Image:            $Image
        Amount of likes:  $Likes
        Comments:         $Comments
        Share:        $Shares
        Link:         $Link
      --%>
      <a href="$Link" target="_blank">
        <div title="$Description.LimitCharacters(45)" style="width: 100px; height: 100px; display: inline-block; background-image: url($Image); background-size: cover; background-position: center"></div>
      </a>

    <% end_loop %>

  <% else %>
    <p>Sorry, there is no available posts at the moment</p>
  <% end_if %>

<% else %>
  <p style="color: red; font-style: italic">$message</p>
<% end_if %>
