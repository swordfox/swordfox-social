<?php

use Facebook\Facebook;
use GuzzleHttp\Client;
use SilverStripe\View\ArrayData;
use SilverStripe\ORM\DataExtension;
use Psr\SimpleCache\CacheInterface;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\ORM\ArrayList;
use SilverStripe\Core\Config\Config;
use SilverStripe\View\SSViewer;
use SilverStripe\View\ThemeResourceLoader;
use SilverStripe\Core\Convert;
use SilverStripe\ORM\FieldType\DBHTMLText;

class InstagramExtension extends DataExtension
{
    protected $guzzle;

    protected $cache;

    protected $config;

    protected $INSTAGRAM_PAGE_ID;

    protected $INSTAGRAM_ACCESS_TOKEN;

    protected $INSTAGRAM_GROUP_URL;

    protected $INSTAGRAM_POST_LIMIT = 9;

    public function __construct()
    {
        parent::__construct();

        $this->config = Config::inst()->get('SwordfoxSocial');

        if(isset($this->config['Instagram']))
        {
            $this->guzzle = new Client();
            
            $this->INSTAGRAM_PAGE_ID = $this->config['Instagram']['PAGE_ID'];
            $this->INSTAGRAM_ACCESS_TOKEN = $this->config['Instagram']['ACCESS_TOKEN'];
            $this->INSTAGRAM_GROUP_URL = $this->config['Instagram']['GROUP_URL'];

            $this->cache = Injector::inst()->get(CacheInterface::class . '.apiInstagramCache');
            // $this->cache->clear();
        }
    }

    public function InstagramFeed()
    {
        if(!$this->cache->has('inst-feed-posts'))
        {
            $fb = new Facebook([
                'app_id' => $this->config['Facebook']['APP_ID'],
                'app_secret' => $this->config['Facebook']['APP_SECRET'],
                'default_graph_version' => 'v4.0'
            ]);

            try {

                $response = $fb->get(
                    $this->config['Instagram']['PAGE_ID'].'/media?fields=id,media_url,thumbnail_url,media_type,caption,timestamp,permalink,like_count,comments_count&limit='.$this->config['Instagram']['LIMIT'],
                    $this->config['Facebook']['ACCESS_TOKEN']
                );

            } catch (FacebookExceptionsFacebookResponseException $e) {

                echo 'Graph returned an error: ' . $e->getMessage();
                exit;

            } catch (FacebookExceptionsFacebookSDKException $e) {

                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;

            }

            if($response && !$response->isError())
            {
                $feed = json_decode($response->getBody()); // $response->getGraphEdge()

                $this->cache->set('inst-feed-posts', json_encode([
                  'status'      => true,
                  'posts'       => $feed->data,
                  'countposts'  => count($feed->data),
                  // 'next'        => $feed->paging->next
                ]), 86400);
            }
            else
            {
                $this->cache->set('inst-feed-posts', json_encode([
                  'status'      => false,
                  'message'     => 'SDK error'  // $response->getReasonPhrase()
                ]), 86400);
            }
        }

        $result = json_decode( $this->cache->get('inst-feed-posts') );

        $data = new ArrayData( $this->templateResponse($result) );

        return $data->renderWith('InstagramFeed');
    }

    private function templateResponse($result)
    {
        $themeDir = ThemeResourceLoader::inst()->getThemePaths(SSViewer::get_themes())[0];

        $posts = new ArrayList();

        if($result->status)
        {
            $i = 1;

            foreach($result->posts as $post)
            {
                // var_dump($post);exit;

                if($i > $this->INSTAGRAM_POST_LIMIT)
                {
                    break;
                }

                $text = empty($post->caption) ? '' : $post->caption;
                // $text = preg_replace('~#[\w-]+~', '', $text); // remove hashtags
                $text = preg_replace('/\s+/', ' ', trim($text)); // remove spaces and line breaks
                $text = preg_replace('/^(\S+(\s+\S+(\s+\S+(\s+\S+)))?)/', '<strong>$1</strong>', $text);

                if(strlen($text) > 140)
                {
                  $text = substr($text, 0, 140) . '...';
                }

                $textOutput = DBHTMLText::create();
                $textOutput->setValue($text);
                $posts->push(new ArrayData([
                    'Date'        => date('Y-m-d', strtotime($post->timestamp)),
                    'Image'       => $post->media_type == 'VIDEO' ? $post->thumbnail_url : $post->media_url,
                    'Link'        => $post->permalink,
                    'Text'        => $textOutput,
                    'Likes'       => $post->like_count,
                    'Comments'    => $post->comments_count,
                    'Type'        => $post->media_type,
                    // 'Tags'        => implode($post->tags, ', ')
                ]));

                $i++;
            }

            $result->posts = $posts;

            return $result;
        }
        else
        {
            return $result;
        }
    }
}