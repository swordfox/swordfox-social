<?php

use GuzzleHttp\Client;
use SilverStripe\View\ArrayData;
use SilverStripe\ORM\DataExtension;
use Psr\SimpleCache\CacheInterface;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\ORM\ArrayList;
use SilverStripe\Core\Config\Config;
use SilverStripe\View\SSViewer;
// use SilverStripe\View\ThemeResourceLoader;

class FacebookExtension extends DataExtension
{
    protected $guzzle;

    protected $cache;

    protected $config;

    protected $FACEBOOK_PAGE_ID;

    protected $FACEBOOK_ACCESS_TOKEN;

    protected $FACEBOOK_GROUP_URL;

    protected $FACEBOOK_LIMIT;

    protected $FACEBOOK_GRAPH_URL = 'https://graph.facebook.com';

    protected $FACEBOOK_FIELDS = [
      'created_time',
      'message',
      'permalink_url',
      'comments.limit(1).summary(true)',
      'likes.limit(1).summary(true)',
      'shares',
      'full_picture'
    ];

    public function __construct()
    {
        parent::__construct();

        if(!isset($this->config['Facebook']))
        {
            $this->config = Config::inst()->get('SwordfoxSocial');

            $this->guzzle = new Client();

            $this->FACEBOOK_PAGE_ID = $this->config['Facebook']['PAGE_ID'];
            $this->FACEBOOK_ACCESS_TOKEN = $this->config['Facebook']['ACCESS_TOKEN'];
            $this->FACEBOOK_GROUP_URL = $this->config['Facebook']['GROUP_URL'];
            $this->FACEBOOK_LIMIT = $this->config['Facebook']['POST_LIMIT'];

            $this->cache = Injector::inst()->get(CacheInterface::class . '.apiFacebookCache');
        }
    }

    public function FacebookFeed($limit = null)
    {
        if(!$this->cache->has('fb-feed-posts'))
        {
            $response = $this->guzzle->get($this->FACEBOOK_GRAPH_URL . '/' .$this->FACEBOOK_PAGE_ID . '/feed', [ 'query' => [
                'access_token' => $this->FACEBOOK_ACCESS_TOKEN,
                'limit'        => $this->FACEBOOK_LIMIT,
                'fields'       => implode($this->FACEBOOK_FIELDS, ',')
            ]]);

            if($response->getStatusCode() == 200)
            {
                $feed = json_decode($response->getBody());

                $this->cache->set('fb-feed-posts', json_encode([
                  'status'      => true,
                  'posts'       => $feed->data,
                  'countposts'  => count($feed->data),
                  'next'        => $feed->paging->next
                ]));
            }
            else
            {
                $this->cache->set('fb-feed-posts', json_encode([
                  'status'      => false,
                  'message'     => $response->getReasonPhrase()
                ]));
            }
        }

        $result = json_decode( $this->cache->get('fb-feed-posts') );

        $data = new ArrayData( $this->templateResponse($result, $limit) );

        return $data->renderWith('FacebookFeed');
    }

    private function templateResponse($result, $limit = null)
    {
        // $themeDir = ThemeResourceLoader::inst()->getThemePaths(SSViewer::get_themes())[0];

        $posts = new ArrayList();

        if($result->status)
        {
            $i = 1;

            foreach($result->posts as $post)
            {
                $noImage = false;

                if($i > $this->FACEBOOK_LIMIT)
                {
                    break;
                }

                $id = $post->id;

                $pubdate = $post->created_time;
                $postdate = date('d F\ Y', strtotime($pubdate));

                if(@$post->full_picture && property_exists($post, 'full_picture')) // !strstr($post->full_picture, 'safe_image.php')
                {
                    $image = $post->full_picture;
                }
                else
                {
                    $noImage = true;
                    $image = isset($this->config['Base']['DEFAULT_IMAGE']) ? $this->config['Base']['DEFAULT_IMAGE'] : '/swordfox-social/no-image.jpg';
                }

                $message = property_exists($post, 'message') ? $post->message : '';

                $permalink_url = property_exists($post, 'permalink_url') ? $post->permalink_url : '';
                $content = strip_tags($message,'<strong><b><i><u>');
                $likes = property_exists($post, 'likes') ? $post->likes->summary->total_count : 0;
                $shares = property_exists($post, 'shares') ? $post->shares->count : 0;
                $comments = property_exists($post, 'comments') ? $post->comments->summary->total_count : 0;

                $content = trim(preg_replace('/\s+/', ' ', $content));

                $permalink_url = ($permalink_url == '' ? $this->FACEBOOK_GROUP_URL : $permalink_url);

                $posts->push(new ArrayData([
                    'Date'        => $postdate,
                    'Image'       => $image,
                    'NoImage'     => $noImage,
                    'Link'        => $permalink_url,
                    'Description' => $content ?: '-',
                    'Likes'       => $likes,
                    'Shares'      => $shares,
                    'Comments'    => $comments,
                ]));

                if($limit && $limit == $i)
                {
                    break;
                }

                $i++;
            }

            $result->posts = $posts;

            return $result;
        }
        else
        {
            return $result;
        }
    }
}
