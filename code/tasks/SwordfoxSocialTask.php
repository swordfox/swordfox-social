<?php

use SilverStripe\Dev\BuildTask;
use Psr\SimpleCache\CacheInterface;
use SilverStripe\Core\Injector\Injector;

class SwordfoxSocialTask extends BuildTask
{
    private static $segment = 'SwordfoxSocialTask';

    protected $title = 'Swordfox-social clearing';

    protected $description = 'Clear all cache of swordfox-social plugin';

    protected $enabled = true;

  	public function run($request)
    {
        $cache = Injector::inst()->get(CacheInterface::class . '.apiFacebookCache');
        $cache->delete('fb-feed-posts');
  	}
}
